走过路过，不要错过，动动鼠标，点一下右上角的star。

有需要的可以加q群：962963257

[zotero基础配置](notes/zotero基础配置.md)

[zotero插件介绍](notes/zotero插件介绍.md)

[zotero插件的安装步骤](notes/zotero如何安装插件.md)

[插件的谷歌翻译无法使用，使用其他的翻译引擎教程](notes/小牛翻译.md)

[zotero使用teracloud同步](notes/teracloud.md)

[scholar sematic详情页的引用文献抓取和搜索页的批量抓取](https://www.bilibili.com/video/BV1YT41137BJ)

[zotero浏览器插件抓取知网等国内网页](notes/zotero浏览器插件抓取知网等国内网页.md)

[zotero为什么不能下载知网pdf说明](notes/zotero为什么不能下载知网pdf.md)

[关于zotero云同步的问题](notes/关于zotero云同步的问题.md)

[pad同步pdf](notes/pad上看不到pdf.md)

[zotero_beta_quicklook](https://www.bilibili.com/read/cv15720656 )

[插件失效了怎么办，如何自己下载插件](notes/如何自己下载插件.md)

[zotero中英文混排](notes/zotero中英文混排.md)

[使用引用文献格式文件csl](notes/如何使用引用文献格式文件.md)

[zotero利用better BibTex引用文献](notes/zotero配合better_bibtex引用文献.md)

[zotero和obsidian联动，按照模板创建笔记](notes/zotero-obsidian.md)

[知网下载硕博论文pdf](notes/知网下载硕博论文pdf.md)

[调整word引文格式间距](notes/自己修改引文格式文件.md)

[可视化自定义引文格式的网站](https://editor.citationstyles.org/about/)

[zotero像endnote一样引用文献](notes/zotero像endnote一样引用文献.md)

[坚果云 | Papership或Zotero使用webDAV验证服务器不成功怎么办？](https://www.jianshu.com/p/880ab833a0ba)

[如何打包插件](notes/如何用源码打包自己的插件.md)

[wps使用zotero完美解决方案](notes/在WPS中使用Zotero_Word插件.html) 援引自https://zotero.yuque.com/staff-gkhviy/zotero/dh3ahi?#CkEeS

[换电脑迁移数据和插件](notes/换电脑迁移数据.md)

zotero配合quicker使用（仅win）：

1. [zotero在word中引用文献的快捷功能](https://getquicker.net/Sharedaction?code=4a4a903b-be9d-490f-560d-08d9eec3922c)
2. [将zotero的条目链接填到obsidian](https://getquicker.net/Sharedaction?code=3128dcf1-af70-4fd3-5631-08d9eec3922c)
3. [zotero_beta划词翻译](notes/zotero_beta划词翻译.md)
4. [zotero在obsidian里面填充doi链接](https://getquicker.net/Sharedaction?code=fd3e105b-d423-437f-db42-08d9f27e1fa6)

## 给刚考上研究生的小伙伴

如果你是一个刚考上研究生但是一脸迷茫，甚至查询论文都有困难的小伙伴，希望下面的网址对你有所帮助

如果有些链接打不开请将该仓库下载下来，然后阅读里面的`README.md`文件，该文件的打开软件是`软件`文件夹里面的`typora.exe`（一种markdown编辑器）

[论文查询网站](notes/论文查询网站.md)

