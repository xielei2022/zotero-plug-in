如果你觉得坚果云空间1g，上传流量每月1g，下载流量3g不够使用的话可以使用一个日本的网盘，支持teracloud来实现zotero多设备多平台同步（win、Mac、iOS、Android）

[teracloud网址](https://teracloud.jp/en/)

![image-20221101112355043](teracloud.assets/image-20221101112355043.png)

如果你没有账号的话，点击`create account`注册账号

![image-20221101112459546](teracloud.assets/image-20221101112459546.png)

然后会收到一封电子邮件，跟着操作就行

登录账号

点击`My Page`

![image-20221101112748534](teracloud.assets/image-20221101112748534.png)

将我的推荐码`QP5DP`填进去，这样可以多5g，一共就有15g的空间，足够读研使用了，<strong style="color:#c00000;">免费用户如果两年没有登录，官方会删除你的数据</strong>

![image-20221101112828984](teracloud.assets/image-20221101112828984.png)

在apps connection 勾选`Turn on Apps Connection`

![image-20221101113550189](teracloud.assets/image-20221101113550189.png)

复制`URL、ID、Apps Password`，这里的Apps Password只会出现一次，记得保存下来，如果想要再显示就会重置。

设置zotero同步

`编辑-首选项-同步`



![image-20221101113841365](teracloud.assets/image-20221101113841365.png)

将前面的URL填进去，<strong style="color:#c00000;">注意删掉填进去的链接里面的https</strong>，填写前面的id和复制的`apps password`，点击下面的OK，再进来点击`验证服务器`，这样就弄好同步了，再另一台电脑上也同样操作。