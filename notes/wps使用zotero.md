## 下载扩展

下载word扩展，https://gitee.com/tughv/zotero-word-for-windows-integration/tree/master/install或者https://github.com/zotero/zotero-word-for-windows-integration/tree/master/install或者[阿里云](https://www.aliyundrive.com/s/n2tAo462HX6)

下载`zotero.dotm`

![image-20221103110332457](wps%E4%BD%BF%E7%94%A8zotero.assets/image-20221103110332457.png)

本教程会打包到压缩包里面

将下载好的`zotero.dotm`放到zotero安装文件夹的startup文件夹

![image-20221103110939772](wps%E4%BD%BF%E7%94%A8zotero.assets/image-20221103110939772.png)

![image-20221103111038752](wps%E4%BD%BF%E7%94%A8zotero.assets/image-20221103111038752.png)

重启wps

## 下载vba运行环境

VBA_Setup.exe https://www.aliyundrive.com/s/KWy7Hcjpyzg 点击链接保存，或者复制本段内容，打开「阿里云盘」APP ，无需下载极速在线查看，视频原画倍速播放。

双击运行，<strong style="color:#c00000;">没有任何提示</strong>





如果还是没有去加载项里面启动一下

![image-20221103111435029](wps%E4%BD%BF%E7%94%A8zotero.assets/image-20221103111435029.png)

![image-20221103111338460](wps%E4%BD%BF%E7%94%A8zotero.assets/image-20221103111338460.png)

本人在使用过程中发现，如果直接使用wps插入文献会出现一个如下的bug,点击否就行，而且每次插入文献都会出现。

![image-20221103111559830](wps%E4%BD%BF%E7%94%A8zotero.assets/image-20221103111559830.png)

但是如果你先用word插入一篇文献后再用wps就可以正常使用了