<center>zotero如何安装插件</center>

1. `工具-插件`

   ![image-20220320174628230](zotero如何安装插件.assets/image-20220320174628230.png)

2. 将插件拖进来，然后点击`Restart now`，就会重启zotero。![image-20220320174759367](zotero如何安装插件.assets/image-20220320174759367.png)
3. 或者点击`齿轮-Install Add-on From File`，在弹出的文件夹窗口选择你的插件![image-20220320174818815](zotero如何安装插件.assets/image-20220320174818815.png)
4. 点击插件上方的`Restart now`